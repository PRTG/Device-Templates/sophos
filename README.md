Sophos tempate for PRTG.
===========================================

This project contains all the files necessary to create sensors for Sophos UTM9 and Sophos XG from a template.

[This template is a further development based on Felix Saure's initial version of Sophos template in Paessler KB article #73179](https://kb.paessler.com/en/topic/73179-how-can-i-monitor-sophos-utm-devices-with-prtg)


![Sophos UTM9 Customer sensor](./Images/UTM9-Sensors.png)
Sophos UTM9

![Sophos XG Customer sensor](./Images/XG-Sensors.png)
Sophos XG

Download Instructions
=========================
 [A zip file containing all the files in the installation package can be downloaded directly using this link](https://gitlab.com/PRTG/Device-Templates/sophos/-/jobs/artifacts/master/download?job=PRTGDistZip).

Installation Instructions
=========================
Please see: [Generic PRTG custom sensor instructions INSTALL_PRTG.md](./INSTALL_PRTG.md)

UTM9 Sensors
====
![UTM9 Health Sensor](./Images/UTM9-Health.png)
Health Sensor

The template also creates several sensors based on generic Unix Linux OID...

XG Sensors
=====
![XG Health Sensor](./Images/XG-Health.png)
XG Health Sensor


![XG Netowrk Services Sensor](./Images/XG-Net-svc-Stat.png)
XG Netowrk Services Sensor

![XG System Services Sensor](./Images/XG-Sys-svc-Stat.png)
XG System Services Sensor

